# pyfx

This python script accepts an image as input and generates a new image with an effect applied according to the specified algorithm. There are currently three algorithms available (circles, pixels, and triangles), and you are also able to specify the size of the generated elements.

The included [photo](https://unsplash.com/photos/G0066DUsxpg) was taken by [Owlie Harrington](https://unsplash.com/@pressrestart) and is available for use under the terms of the [Unsplash License](https://unsplash.com/license).

## Input
![Input image](input/input.jpg)

## Output
### Circles
![Output image](output/output_circles.jpg)

### Pixels
![Output image](output/output_pixels.jpg)

### Triangles
![Output image](output/output_triangles.jpg)
